/* eslint-disable no-unused-expressions */


import React from 'react';
import {Tor as Net,Disk} from 'otag';
import './Login.scss';


export default class Login extends React.Component{
  socket;
  constructor(props) {
    super(props)
    this.login = {
      name: null
    }
    props.refr
      ? props.refr(this)
      : null
  }
  send(){
    console.log("name".of(this.login))
    Net
      .req("login", this.login)
      .catch((...args) => console.log(args) || (Disk.key=this.login.name)&& this.props.onLogin())
  }
  render() {
    return (
      <div class="Login">
        <h1> Login </h1> 
        <input type="text" placeholder="name" onChange={({target:{value}}) => this.login.name=value}/>
        <button onClick={this.send.bind(this)}>Login</button>
      
      </div>
    );
  }
}
