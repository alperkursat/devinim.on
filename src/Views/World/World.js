/* eslint-disable no-unused-expressions */
import React from 'react';
import { Link } from "react-router-dom";
import { LineChart, Line } from 'recharts';
import Disk from 'o.disk'
import DeviceOrientation from 'react-device-orientation';

import Compass from '../../Components/Compass/Compass';
import './World.scss';
Disk.route=[]
let randColor = ()=>["#881","#818","#188","#841"][Math.round(Math.random()*1e8)%4]
let motional= [0];
export default class App extends React.Component{
  socket;
  constructor(props) {
    super(props)
    
    this.users={}
    this.state = {alpha: 5,users:this.users}
    this.colors={}
    this.socket=props.socket
    this.socket.on('motion', (alpha)=>{
      alpha =Number(alpha.toFixed(2))
      alpha !== this.state.alpha?this.setState({alpha})||motional.push(alpha):null
      this.forceUpdate()
    });
    this.socket.on("join",(m) => {
      this.colors[m] = randColor()
      this.users[m] = 0
      this.forceUpdate()
    })
    this.socket.on("users",(m)=>{
      this.users=m.reduce((usrs,i)=>({...usrs,[i]:0}),{})
      this.forceUpdate()
    })
    this.socket.on("exit",(m)=>{
      delete this.colors[m]
      delete this.users[m]
      this.forceUpdate()
    })
    this.socket.on("deg",([u,deg])=>(this.colors[u]=randColor())&&(this.users[u]=deg))
    props.refr
      ? props.refr(this)
      : null

    this.self={
      _alpha: 0,
      get alpha(){return this._alpha},
      set alpha(a){
        a = Number(a.toFixed(2))
        if(a!==this._alpha){
          console.log(a)
          this._alpha = a
          Disk.route=Disk.route.concat([a])
          props.socket
            ? props.socket.emit("motion",{ a })
            : null
          }
          
      }
    }
  }
  render() {
    return (
      <div className="App">
        <div className="reflect">
          Alpha: { this.state.alpha}
        </div>  
        <Compass alpha={this.state.alpha} size={100}/>
        <DeviceOrientation>
          {({alpha}) => {
            this.self.alpha = alpha
            
            return null}}
        </DeviceOrientation>
        <div className="user List">
          {Object.keys(this.users).reduce((l,name)=>l.concat(
              <Compass alpha={this.users[name]} size="30" color={this.colors[name]}/>,
              <a>{name}</a>
          ),[])}
        </div>
        <svg stroke="#09F" fill="transparent" strokeLinejoin="round" strokeWidth="5" height="512pt" viewBox={(motional.length>512?motional.length-512:0)+" -180 512 512"} width="512pt" xmlns="http://www.w3.org/2000/svg">
          <path d={ "M0 "+motional.join("h1 V")} stroke="#F90"/>
          <path d={ "M0 "+Disk.route.join("h1 V")}/>
          </svg>
      </div>
    )
  }
}