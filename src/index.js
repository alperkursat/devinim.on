import React from 'react';
import ReactDOM from 'react-dom';
import Disk from 'o.disk';
import './index.css';
import App from './Views/App/App';
import World from './Views/World/World';
import Login from './Views/Login/Login';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import * as serviceWorker from './serviceWorker';
import io from 'socket.io-client/dist/socket.io';

const connectionConfig = {
  jsonp: false,
  reconnection: true,
  reconnectionDelay: 100,
  transports: ['websocket'],
  query: "key".of(Disk),
  reconnectionAttempts: 100000
};
let socket = io("http://localhost:3001", connectionConfig);
socket.on('connect', function(){
  console.log('Socket connected!');
});
let render = ()=>
ReactDOM.render(

<Router>
      <div>
      {Disk.key
      ? (<Route exact path="/" render={(props)=>(<World {...props} {...{socket}} alpha="15" />)}/>)
      : (<Route exact path="/" render={(props)=>(<Login {...props} onLogin={()=>render()} />)}/>)
      }
      </div>
    </Router>, document.getElementById('root'));

render()
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
//<App refr={ref=>(app=ref)&&(app.socket=socket)}/>